extern crate itertools;

use itertools::Itertools;
use itertools::structs::Combinations;
use std::slice::Iter;
use std::path::Path;

pub fn paths(source_dir: String) -> Vec<String> {
    let  d = std::fs::read_dir( Path::new(&source_dir)).unwrap();
    let mut v = Vec::new();
    d.for_each(|f|{
        let fname = f.unwrap().path();
        v.push(String::from(fname.to_str().unwrap()));
    });
    v
}

/// Return combinations
pub fn calc(v: &Vec<String>, n: usize) -> Combinations<Iter<String>>{
    v.iter().combinations(n)
}

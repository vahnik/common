use std::fs::File;
use std::path::Path;
use std::io::prelude::*;
use std::error::Error;

pub struct Out {
    file: Option<File>
}

impl Out {
    pub fn new(out_file: Option<&str>) -> Self {
        if out_file.is_some() {
            let path = Path::new(out_file.unwrap());

            let file = match File::create(&path) {
                Err(why) => panic!("couldn't create {}: {}",
                                   path.display(),
                                   why.description()),
                Ok(file) => file,
            };
            Out { file: Some(file) }
        } else {
            Out { file: None }
        }
    }

    pub fn print_to_all(&mut self, text: String) {
        println!("{}", text);
        self.print(text);
    }
    pub fn print(&mut self, text: String) {
        debug!("{}", text);
        match &mut self.file {
            Some(b) => {
                match b.write_all(format!("{}\n",text).as_bytes()) {
                    Ok(t) => {
                        //do nothing i suppose
                    },
                    Err(e) => {
                        error!("no file error:{:?}", b);
                        println!("{}", text);
                    }
                }
            }
            None => {
                println!("{}", text);
            }
        };

    }
}

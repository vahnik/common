use std::collections::BTreeMap;
use std::error::Error;
use std::fs::File;
use std::io::BufRead;

use std::io::BufReader;
use std::io::Read;
use std::iter::FromIterator;
use std::collections::HashMap;
use crate::domain::period::Period;
use crate::domain::bar::Bar;

pub fn parse_from_file(path:&str) -> BTreeMap<Period, Bar> {
    parse_cached(path.to_string())
}
// Parse paths to (paths, bars)
pub fn parse_paths_to_bars(vec: Vec<&String>) -> (Vec<String>, Vec<BTreeMap<Period, Bar>>) {
    let mut cloned = Vec::new();
    let mut bars = Vec::new();
    for p in vec {

        let parsed = parse_from_file(p);

        cloned.push(p.clone());
        bars.push(parsed);
    }
    (cloned, bars)
}

fn bar(csv_row: &str, ticker: &str) -> Result<Bar, Box<Error>> {
    Bar::from_1(csv_row, ticker)
}
fn read_file_buff(path: &str) -> BufReader<File> {
    let mut f = File::open(path).expect(format!("cannot find file:{}", path).as_str());
    BufReader::new(f)
}

fn parse_with_vec(csv: BufReader<File>, ticker: &str) -> BTreeMap<Period, Bar> {
    let bars = csv.lines().filter_map(|line| {
        match bar(&line.expect("cannot read line"), ticker) {
            Ok(b) => Some((Period::from(&b), b)),
            Err(e) => None
        }
    });
    BTreeMap::from_iter(bars.into_iter())
}

fn ticker(path: &str) -> &str {
    path.rsplit("/").next().expect(format!("Cannot parse ticker from path:{}", path).as_str())
}
cached! {
      FIB;
      fn parse_cached(path: String) -> BTreeMap<Period, Bar> = {
//        println!("PARSE:{}", path);
        let ticker = ticker(&path);
        parse_with_vec(read_file_buff(&path), ticker)
      }
    }

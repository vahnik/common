use std::path::Path;
use std::fs::create_dir_all;
use std::fs::File;
use std::fs::read_dir;
/// convert Vec<full_filenames> to filename-filename-..
pub fn filename(paths: &Vec<&String>) -> String {
    paths.iter().map(|f| {
        let p = Path::new(f);
        p.file_name().unwrap().to_str().unwrap()
    }).fold(String::new(), |acc, p| {
        let split = if acc.is_empty() { "" } else { "-" };
        acc + split + p
    })
}
pub fn fullpath_to_filename_without_ext(path: String) -> String {
    let p = Path::new(&path);
    p.file_stem().unwrap().to_str().unwrap().to_string()
}
pub fn create_file_with_dir(target_dir: Option<&str>, filename: &String) -> String {
    match target_dir {
        Some(dir) =>
            {
                if read_dir(Path::new(dir)).is_err() {
                    debug!("create dir:{:?}", dir);
                    create_dir_all(Path::new(dir))
                        .expect(format!("Cannot create dir:{:?}", dir).as_str());
                }
                let name = format!("{}/{}", dir, filename);
                name
            }
        None => filename.clone()
    }
}

use std::collections::BTreeMap;
use std::iter::Map;
use crate::domain::bar::Bar;
use crate::domain::period::Period;

pub fn mean(bars: &BTreeMap<Period, Bar>, period: i32) -> BTreeMap<Period, f64> {
    let mut res = BTreeMap::new();
    let mut curr_mean = 0.;
    let mut frame = 0;
    let mut stack = Vec::new();
    for  b in  bars {
        let c = b.1.avg_price();
        if (frame < period) {
            frame += 1;

        } else {
            stack.remove(0);
        }
        stack.push(c);
        let mut tmp_mean =  stack.iter().sum::<f32>() / stack.len() as f32;
        res.insert(b.0.clone(), tmp_mean as f64);
    }
    res
}
//calc derivative in every point using points in period
//deriv is curr - prev
pub fn derivative(vals: &BTreeMap<Period, f64>) -> BTreeMap<Period, f64> {
    let mut res = BTreeMap::new();
    let mut prev = None;
    for  b in  vals {
        let v = b.1;
        let diff = if (prev.is_none()) {
            0.0
        } else {
            v - prev.unwrap()
        };
        res.insert(b.0.clone(), diff);
        prev = Some(v);
    }
    res
}

#[cfg(test)]
mod tests {
    use super::*;
    use itertools::Itertools;

    #[test]
    fn correct_mean() {

        let rng: Vec<i32> =  (1..11).collect();
        let mut bars = BTreeMap::new();
        let mut expected = Vec::new();
        for ex in rng {
            let p = ex as f32;
            let b = Bar {
                ticker: "test".to_string(),
                year: 2012,
                month: 1,
                day: ex as u8,
                hour: 0,
                min: 0,
                open: p + 0.5,
                high: (p + 1.0),
                low: p,
                close: p + 0.5,
                vol: 0.0
            };
            println!("{}:{:?}", ex, b.avg_price());
            expected.push(b.avg_price() as f64);
            bars.insert(Period::from(&b), b);
        }
        let res = mean(&bars, 5);
        res.iter().for_each(|f|{
            println!("{:?} - {}", f.0, f.1);
        });
        let mean_result:Vec<&f64> = res.values().collect();
        println!("result:{}", mean_result.len());
        let exp_sum_5: f64 = expected.iter().take(5).sum();
        let test_avg_5 = mean_result.get(4).unwrap();
        assert_eq!(bars.len(), mean_result.len());
        assert_eq!((expected.get(0).unwrap() + expected.get(1).unwrap()) / 2.0,
               **mean_result.get(1).unwrap());
        assert_eq!(exp_sum_5 / 5.0, **test_avg_5);
        let mut exp_avg_5_10 = 0.0;
        for i in 5..10 {
            exp_avg_5_10 += *expected.get(i).unwrap();
        }
        exp_avg_5_10 = exp_avg_5_10 / 5.0;
        assert_eq!(exp_avg_5_10, **mean_result.last().unwrap());
    }
}


use std::collections::BTreeMap;
use std::iter::Map;
use crate::domain::bar::Bar;
use crate::domain::period::Period;

//merge bars with option None for absent periods
pub fn merge(b1: &BTreeMap<Period, Bar>, b2: &BTreeMap<Period, Bar>)
                     -> BTreeMap<Period, (Option<Bar>, Option<Bar>)> {
    let mut res = BTreeMap::new();
    for bar in b1 {
        let p = bar.0;
        let b = bar.1;
        res.insert(Period::from(p), (Some(b.clone()), None));
    }
    for bar in b2 {
        let t1 = bar.0;
        let b = bar.1;
        let e =
            res.entry(Period::from(t1)).or_insert((None, None));
        e.1 = Some(b.clone());
    }
    res
}

